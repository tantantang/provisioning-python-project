class DeviceHostInventory:
	def __init__(self,variable):
		self.config = variable['config']
                self.logger = variable['logger']
                self.cmdb_file = variable['cmdb_file']
                self.zbx_api = variable['zbx_api']
                self.db_obj = variable['db_obj']
	
	def create_inventory(self,device):
		#inventory = []
		inventory = {}
                invent = {}
                for key in self.config['sgts']['inventory']['zabbix_csv']:
			invent[key] = device[self.config['sgts']['inventory']['zabbix_csv'][key]]

		#inventory.append({'inventory_mode':self.config['sgts']['inventory']['inventory_mode'],"inventory":invent})
		inventory = {'inventory_mode':self.config['sgts']['inventory']['inventory_mode'],"inventory":invent}
		#print inventory
		return inventory
