#!/usr/bin/python

from optparse import OptionParser
import os,pymysql,pykwalify,yaml,json,logging,sys
import os.path
from pykwalify.core import Core
sys.path.append('lib/SGTS')
#from SgtsProv import MasterTable
from SgtsProv import ProvisioningLooper
from SgtsProv import Device
from pyzabbix import ZabbixAPI

parser = OptionParser()
parser.add_option("-a", "--script-config", dest="scriptConfig",
                  help="configuration directory of provisioning script", metavar="FILE")
parser.add_option("-b", "--script-schema", dest="scriptSchema",
                  help="Schema directory of provisioning script", metavar="FILE")
parser.add_option("-c", "--zabbix-config", dest="zabbixConfig",
		  help="configuration directory of zabbix", metavar="FILE")
parser.add_option("-d", "--zabbix-schema", dest="zabbixSchema",
		  help="Schema directory of Zabix", metavar="FILE")
parser.add_option("-e", "--mysql-schema", dest="mysqlSchema",
		  help="Schema directory of Mysql", metavar="FILE")
parser.add_option("-f", "--mysql-config", dest="mysqlConfig",
		  help="configuration directory of Mysql", metavar="FILE")
parser.add_option("-g", "--config-dir", dest="configDir",
		  help="configuration directory path", metavar="DIRECTORY")
parser.add_option('-i', '--schema-dir', dest='schemaDir',
		  help='Schema directory', metavar='FILE')
parser.add_option('-u','--user',dest="user",
		  help='User')
parser.add_option('-m','--no-cmdb-retrieval',dest='cmdbRetrieval',
		  help='No CMDB Retrieval')

(options, args) = parser.parse_args()

config_dir = 'etc/sgts/';
schema_dir = 'data/sgts/schemas/';

if options.configDir:
	if (os.path.isdir(options.configDir) == True):
		config_dir = options.configDir;
if options.schemaDir:
	if (os.path.isdir(options.schemaDir)== True):
		schema_dir = options.schemaDir;

######################################################## checking if prov conf exists
scriptConf = config_dir + "sgts-provisioning.yml";
if options.scriptConfig:
	if os.path.isfile(options.scriptConfig):
		scriptConf = options.scriptConfig;

################################################## checking if prov schema exists
scriptYML = schema_dir + "sgts-provisioning.yml";
if options.scriptSchema:
	if os.path.isfile(options.scriptSchema):
		scriptYML = options.scriptSchema;

####################################################### Checking if zabbix conf exists
zabbixConf = config_dir + "zabbix-api.yml";
if options.zabbixConfig:
	if os.path.isfile(options.zabbixConfig):
		zabbixConf = options.zabbixConfig;

############################################################ Checking if zabbix schema exists
zabbixYML = schema_dir + "sgts-zabbix.yml";
if options.zabbixSchema:
	if os.path.isfile(options.zabbixSchema):
		zabbixYML = options.zabbixSchema;

################################################################# Checking if mysql conf exists
mysqlConf = config_dir + "sgts-mysql.yml";
if options.mysqlConfig:
	if os.path.isfile(options.mysqlConfig):
		mysqlConf = options.mysqlConfig;

####################################################### Checking id mysql schema exists
mysqlYML = schema_dir + "sgts-mysql.yml";
if options.mysqlSchema:
	if os.path.isfile(options.mysqlSchema):
		mysqlYML = options.mysqlSchema;


c = Core(source_file=scriptConf, schema_files=[scriptYML])
c.validate(raise_exception=True)

d = Core(source_file=zabbixConf, schema_files=[zabbixYML])
d.validate(raise_exception=True)

e = Core(source_file=mysqlConf, schema_files=[mysqlYML])
e.validate(raise_exception=True)

########################################## Setting Logs ##########################
with open(scriptConf, 'r') as stream:
       scriptConfigData = yaml.load(stream);
scriptConfigData = json.loads(str(json.dumps(scriptConfigData)));
log_directory =  scriptConfigData['sgts']['logs']['log_dir'];
if(os.path.isdir(log_directory) != True):
	command = "mkdir -p " + log_directory;
	os.system(command);
	
sgid = "Admin";
if(options.user):
	sgid = options.user;
log_filename = log_directory + sgid + ".log";
logger = logging.getLogger(__name__)
logging.basicConfig(filename=log_filename,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.INFO);
logger.info('Started writing into logs %s',log_filename)

############################ Establishing Connection to Zabbix ########################
with open(zabbixConf, 'r') as stream:
       zabbixConfigData = yaml.load(stream);
zabbixConfigData = json.loads(str(json.dumps(zabbixConfigData)))
url = zabbixConfigData['zabbix']['api_url']
user = zabbixConfigData['zabbix']['api_user']
password = zabbixConfigData['zabbix']['api_passwd']
zapi = ZabbixAPI(url, '',user, password)
if(zapi):
	logger.info("Connected to Zabbix API Version %s",zapi.api_version())
else:
	logger.info("Could not Connect to Zabbix API Version")

params = {'output':['hostid','name'],'search':{'name':'CUSSGP'}}

#for h in zapi.host.get(params):
#        print(h['hostid']+ "=====" + h['name'])

######################################## No CMDB Retrieval #############################################
cmdb_file = ""
if options.cmdbRetrieval:
	cmdb_file = options.cmdbRetrieval
else:
	workDir = scriptConfigData['sgts']['global']['workdir'] + "" + sgid
	if (os.path.isdir(workDir)== False):	
		logger.info("%s directory doesnt exists",workDir)
	else:
		cmdb_file = workDir + "/SGTS_CMDB_.csv"
################################## Establishing connection to mysql ########################################

with open(mysqlConf, 'r') as stream:
       mysqlConfigData = yaml.load(stream);
mysqlConfigData = json.loads(str(json.dumps(mysqlConfigData)));
# Open database connection
cnx = pymysql.connect(mysqlConfigData['mysql']['db_host'],mysqlConfigData['mysql']['db_user'],mysqlConfigData['mysql']['db_password'],mysqlConfigData['mysql']['db_name']);
# prepare a cursor object using cursor() method
#cursor = cnx.cursor()
cursor = cnx.cursor(pymysql.cursors.DictCursor)
master = {'dbh':cursor,'logger':logger}
#dbObject = MasterTable(master)
dbObject = ProvisioningLooper(master)
#master.get_data(cursor)
#cnx.close()

arguments = {'logger':logger,'config':scriptConfigData,'cmdb_file':cmdb_file,'zbx_api':zapi,'db_obj':dbObject}
looper  = Device(arguments)
looper.run()

